.. _changes:


heliopy_multid 0.0.3 (24/07/2020)
==================================

Second release of *heliopy_multid*.

Features
----------

Enhancements
--------------

Documentation
----------------------
- Add a CHANGELOG

Bug fixes
----------
- Fix index bug in data.util to handle dc_elec product

