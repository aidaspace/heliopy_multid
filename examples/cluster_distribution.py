from heliopy_multid.data import cluster
from datetime import datetime


probe = '1'
instrument = 'cis'
start_time = datetime(2004, 6, 18, 11, 35, 0)
end_time = datetime(2004, 6, 18, 11, 40, 0)

product_des = {
    "dist": "3d_ions__C{}_CP_CIS-HIA_HS_MAG_IONS_PF".format(probe),
    "theta": "theta__C{}_CP_CIS-HIA_HS_MAG_IONS_PF".format(probe),
    "phi": "phi__C{}_CP_CIS-HIA_HS_MAG_IONS_PF".format(probe),
    "energy": "energy_table__C{}_CP_CIS-HIA_HS_MAG_IONS_PF".format(probe)
}

cluster_ion_dist = cluster._load(probe, start_time, end_time,
                                 instrument,
                                 product_id='CP_CIS-HIA_HS_MAG_IONS_PF',
                                 product_list=product_des, try_download=True,
                                 xarray=True)

print(cluster_ion_dist.attrs['3d_ions__C1_CP_CIS-HIA_HS_MAG_IONS_PF'])
