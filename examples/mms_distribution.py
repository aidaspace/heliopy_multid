from heliopy_multid.data import mms
from datetime import datetime


probe = '1'
instrument = 'fpi'
mode = 'fast'
start_time = datetime(2018, 4, 8, 0, 0, 0)
end_time = datetime(2018, 4, 8, 0, 1, 0)

product_des = {
    "dist": "mms{}_des_dist_fast".format(probe),
    "energy": "mms{}_des_energy_fast".format(probe),
    "theta": "mms{}_des_theta_fast".format(probe),
    "phi": "mms{}_des_phi_fast".format(probe)
}

mms_electron_dist = mms.download_files(probe, instrument, mode, start_time,
                                       end_time, product_string='des-dist',
                                       product_list=product_des, xarray=True)

print(mms_electron_dist)
